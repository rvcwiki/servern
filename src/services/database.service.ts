import { Injectable } from '@nestjs/common';
import * as mysql from 'mysql';

@Injectable()
export class DatabaseService {
    private connection: any;
    constructor() {
        this.connection = mysql.createConnection({

            // host: '192.168.13.247',
            // user: 'root',
            // password: 'FikaAdmin',
            // database: 'jiradb',

            host: 'localhost',
            user: 'root',
            password: '',
            database: 'angularjira',

        });
    }
    public getConnection() {
        return this.connection;
    }
}