import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as chalk from 'chalk';
// const chalk = require('chalk');


async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  await app.listen(3000, () => {

    const logo =
      `
''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''/o.'''''''''''''''''''''''
'''''''''''''''''''''''/yyo.''''''''''''''''''''''
''''''''''''''''''''''+yyyys.'''''''''''''''''''''
'''''''''''''''''''''+yyyyyys-''''''''''''''''''''
''''''''''''''''''''+yyyssyyys-''''''''''''''''..'
'''''''''''''''''''+yyys:.oyyys-''''''''''-::-.'''
'''''''''''''''''.+yyys:''.oyyys-''''.:/+/:.''''''
''''''''''''''''.+yyys/''''.oso+-.:+oo/-''''''''''
'''''''''''''''.oyyyy/''''''.::/oss+-'''''''''''''
''''''''''''''.oyyyy/''''''-+oss+:.-''''''''''''''
'''''''''''''.syyyy/''''-+syyo/--/ss:'''''''''''''
''''''''''''.oyyyy+''./osyso:''.oyyys/''''''''''''
'''''''''''-oyyyy+.-+syys+-''''':syyys/'''''''''''
''''''''''-oyyyyo/osyys+-'''''''':syyys/''''''''''
'''''''''-oyyyyysyyys+-''''''''''':syyys/'''''''''
''''''''-syyyyyyyyyo-'''''''''''''':yyyyy/''''''''
'''''''-syyyyyyyyo:''''.--.''''''''':syyyy+'''''''
'''''':syyyyyyys/.'''''''.:/+++/:-..'/syyyy+''''''
''''':syyyyyyyo-'''''''''''''.:+ssssoosyyyyy+'''''
'''':syyyyyyy/'''''''''''''''''''.:+syyyyyyyy+.'''
''''--------.''''''''''''''''''''''''.--------.'''
''''''''''''''''''''''''''''''''''''''''''''''''''`;

    console.log(chalk.default.bold.rgb(255, 0, 0)(logo));

  });
}
bootstrap();
