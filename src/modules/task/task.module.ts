import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { TaskBusiness } from './task.business';
import { ServiceModule } from 'services/service.module';

@Module({
  imports: [ServiceModule],
  controllers: [TaskController],
  providers: [TaskBusiness],
})
export class TaskModule { }
