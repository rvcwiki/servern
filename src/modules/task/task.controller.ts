import { Get, Controller, Response, Param } from '@nestjs/common';
import { TaskBusiness } from './task.business';

@Controller('task')
export class TaskController {
    constructor(private readonly Business: TaskBusiness) { }

    @Get('/:id')
    task(@Response() res, @Param('id') data: any) {
        const id = data.id;
        this.Business.task(id).then(item => {
            res.status(200).json(item);
        }).catch(error => {

        });

        // res.status(200).json(data);
    }

}
