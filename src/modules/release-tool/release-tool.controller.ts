import { Controller, Get, Param, Post, Body, Response } from '@nestjs/common';
import { ReleasetoolBusiness } from './release-tool.buniess';

@Controller('release-tool')
export class ReleasetoolController {
    // constructor(private readonly Business: ReleasetoolBusiness) { }

    // @Post()
    // releasetool(@Response() res, @Body() data) {
    //     const action = data.action;
    //     const el = data.el;
    //     this.Business.releasetool(action, el).then(item => {
    //         res.status(200).json(item);
    //     }).catch(error => {

    //     });
    // }

    constructor(private readonly business: ReleasetoolBusiness) { }

    @Get('/:id')
    releasetool(@Response() res, @Param('id') data: any) {
        const id = data;

        console.log(id);

        this.business.releasetool(id).then(item => {
            res.status(200).json(item);
        }).catch(error => {
            res.status(500).json(data);
        });

    }

    @Post()
    releasetoolsong(@Response() res, @Body() data) {
        const asd = data;

        console.log(asd);

        this.business.releasetoolsong(asd).then(item => {
            res.status(200).json(item);
        }).catch(error => {
            res.status(500).json(data);
        });

    }
}
