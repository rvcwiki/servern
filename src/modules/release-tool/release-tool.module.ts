import { Module } from '@nestjs/common';
import { ReleasetoolController } from './release-tool.controller';
import { ReleasetoolBusiness } from './release-tool.buniess';
import { ServiceModule } from 'services/service.module';

@Module({
    imports: [ServiceModule],
    controllers: [ReleasetoolController],
    providers: [ReleasetoolBusiness],
})
export class ReleasetoolModule { }
