import { Controller, Get, Response, Param } from '@nestjs/common';
import { EpicsBusiness } from './epics.business';

@Controller('sss')
export class EpicsController {
    constructor(private readonly Epics: EpicsBusiness) { }

    @Get('/:id')
    EpicsBusiness(@Response() res, @Param('id') data: any) {
        const id = data;
        console.log(id)
        this.Epics.epics(id).then(item => {
            res.status(200).json(item);
        }).catch(error => {
            res.status(500).json(data);
        });

    }
}
