import { Controller, Post, Body, Response } from '@nestjs/common';
import { SearchBusiness } from './search.business';

@Controller('search')
export class SearchController {
    constructor(private readonly Business: SearchBusiness) { }

    @Post()
    SearchBusiness(@Response() res, @Body() data) {
        const dataRequest = data.dataRequest;
        this.Business.search(dataRequest).then(item => {
            res.status(200).json(item);
        }).catch(error => {

        });

        // res.status(200).json(reportdata);

    }

}
